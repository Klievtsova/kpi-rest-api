from bottle import request, response, route, template
from rest_api import app
import json
from my_packages.graph_generation import generate_graph, extract_bpmn_tasks, extract_graphviz_tasks, extract_mermaid_tasks, validate_mermaid
from my_packages.chatbot_api import extract_tasks, convert_to_list, generate_description, ask_gpt_system
from my_packages.kpis import get_similarity, set_overlap_rest, readability, quantitative
from my_packages.augmented_set import augmentation_set
from my_packages.llama_hf import single_prompt_llama, multi_prompt_llama
from my_packages.rules import mermaid
from unit_tests import execute_all_tests
import uuid

def prepare_tasks(data):
    description = data['description']
    gpt_model = data['gpt_model']
    tasks = extract_tasks(gpt_model,description,1)
    return tasks

@route('/docu')
def list():
    tmpl = "<!DOCTYPE html><html><h1>List of all available routes: </h1><ul>"
    route_list = app.routes
    for r in route_list:
        tmpl += "<li>{} {}</li>".format(r.method,r.rule)
    tmpl += "</ul></html>"
    response.set_header('Content-Type','text/html; charset=utf-8') 
    return tmpl

@route('/test')
def test():
    return { "status": "OK!" }

@route('/unit_tests')
# curl --header "Content-Type: application/json"   --request GET    http://0.0.0.0:7070/unit_tests
def unit_tests():
    report = execute_all_tests()
    # tmpl = "<!DOCTYPE html><html>" 
    # tmpl += "<style>table,th,td{ border: 1px solid black;border-collapse: collapse;width: 100%;}</style>"
    # tmpl += "<h2>Unit Tests Report</h2><table><tr><th>Route</th><th>Response</th></tr>"
    # for r in report:
    #     if "200" in r["response"]:
    #         tmpl+="<tr style='background-color:#82e0aa;' ><td>{}</td><td>{}</td></tr>".format(r['route'],r['response'])
    #     else:
    #         tmpl+="<tr style='background-color: #ec7063 ;' ><td>{}</td><td>{}</td></tr>".format(r['route'],r['response'])
    # tmpl+="</table></html>"
    # response.set_header('Content-Type','text/html; charset=utf-8')
    # print(tmpl)
    # return tmpl
    return {"report":report}

@route('/tasks', method=['GET','POST'])
# curl --header "Content-Type: application/json"   --request GET   --data '{"description":"task one is get money, and task two is to buy a new pair of shoes.","gpt_model":"gpt-3.5-turbo-instruct"}'   http://0.0.0.0:7070/task
def get_tasks():
    try:
        data = request.json
        tasks = prepare_tasks(data)
        return { "tasks": tasks }
    except:
        response.status = 400

@route('/task_list', method=['GET','POST'])
# curl --header "Content-Type: application/json"   --request GET   --data '{"description":"task one is get money, and task two is to buy a new pair of shoes.","gpt_model":"gpt-3.5-turbo-instruct"}'   http://0.0.0.0:7070/task_list
def get_task_list():
    try:
        data = request.json
        tasks = prepare_tasks(data)
        tasks = convert_to_list(tasks)
        return tasks
    except:
        response.status = 400

@route('/kpis', method=['GET'])
# curl --header "Content-Type: application/json"   --request GET    http://0.0.0.0:7070/kpis
def get_kpis():
    return {"available_kpis":{"1":"text_similarity","2":"set_overlap","3":"readability_test","4":"quantitative_data",}}

@route('/kpis/text_similarity', method=['GET','POST'])
# curl --header "Content-Type: application/json"   --request GET   --data '{"string_1":"task one is get money, and task two is to buy a new pair of shoes.","string_2":"There are 2 tasks in the process: get money and buy shoes.","sim_type":"bert"}'   http://0.0.0.0:7070/kpis/text_similarity
def get_text_similarity():
    try:
        data = request.json
        if data["string_1"] == "" or data["string_2"] == "":
            response.status = 400
            return {"message": "At least one of your inputs is empty!!!"}
        else:
            sim_type = data["sim_type"]
            if sim_type == "cos" or sim_type == "bert":
                sim_score = get_similarity(data["string_1"],data["string_2"],sim_type)
                return  {"similarity_score": sim_score, "similarity_type": sim_type}
            else:
                response.status = 400
                return {"message": "Use type 'cos' for non-contextual similarity and 'bert' for contextual one!!!"}
    except:
        response.status = 400

@route('/kpis/set_overlap', method=['GET','POST'])
# curl --header "Content-Type: application/json"   --request GET   --data '{"list_1":["go to the shop","buy shoes","go home"],"list_2":["go home","buy bread"], "sim_type":"bert"}'   http://0.0.0.0:7070/kpis/set_overlap
def get_set_overlap():
    # try:
    data = request.json
    if data["list_1"] == "" or data["list_2"] == "":
        response.status = 400
        return {"message": "At least one of your inputs is empty!!!"}
    else:
        sim_type = data["sim_type"]
        if sim_type == "cos" or sim_type == "bert":
            overlap = set_overlap_rest(data["list_1"],data["list_2"],sim_type)
            return  {"set_overlap": overlap, "similarity_type": sim_type}
        else:
            response.status = 400
            return {"message": "Use type 'cos' for non-contextual similarity and 'bert' for contextual one!!!"}
    # except:
    #         response.status = 400

@route('/kpis/readability_test', method=['GET','POST'])
# curl --header "Content-Type: application/json"   --request GET   --data '{"text":"task one is get money, and task two is to buy a new pair of shoes."}'   http://0.0.0.0:7070/kpis/readability_test
def get_readability():
    try:
        data = request.json
        if data["text"] == "":
            response.status = 400
            return {"message": "There is no text!!!"}
        else:
            readability_scores = readability(data["text"])
            return readability_scores
    except:
        response.status = 400

@route('/kpis/quantitative_data', method=['GET','POST'])
# curl --header "Content-Type: application/json"   --request GET   --data '{"text":"task one is get money, and task two is to buy a new pair of shoes."}'   http://0.0.0.0:7070/kpis/quantitative_data
def get_readability():
    try:
        data = request.json
        if data["text"] == "":
            response.status = 400
            return {"message": "There is no text!!!"}
        else:
            quantitative_data = quantitative(data["text"])
            return quantitative_data
    except:
        response.status = 400

#TODO: add another augmentation methods
@route('/augmentation', method=['GET','POST'])
# curl --header "Content-Type: application/json"   --request GET   --data '{"text":"task one is get money, and task two is to buy a new pair of shoes."}'   http://0.0.0.0:7070/augmentation
def get_augmentation():
    data = request.json
    augmented_set = augmentation_set(data["text"])
    return augmented_set

@route('/model', method=['POST'])
# curl --header "Content-Type: application/json"   --request POST   --data '{"description":"task one is get money, and task two is to buy a new pair of shoes.","graph_type":"mermaid.js"}'   http://0.0.0.0:7070/model
def create_model():
    status = 0
    try:
        data = request.json
    except:
        response.status = 400
    if data is None:
        data = request.forms
        #response.status = 400
    description = data['description']
    graph_type = data['graph_type']
    if description == "" or graph_type == "":
        response.status = 400
        return {"message": "At least one of the parameters is missing!!!"}

    #while status != 1:
    graph = generate_graph(description,"rules",graph_type,"gpt-4")
    #    svg_name = uuid.uuid4().hex.upper()[0:11]
    #    status = validate_mermaid(svg_name,graph)

    return {'model': graph, "description": description}

@route('/update_model', method=['POST'])
def update_model():
    status = 0
    try:
        data = request.json
    except:
        response.status = 400
    if data is None:
        data = request.forms
        #response.status = 400
    model = data['model']
    graph_type = data['graph_type']
    update = data['update']
    if model == "" or graph_type == "" or update == "":
        response.status = 400
        return {"message": "At least one of the parameters is missing!!!"}
    prompt = "Consider following {} process model: {}. Update this model: {}.".format(graph_type,model,update)
    #while status != 1:
    new_model = ask_gpt_system(prompt,"gpt-4",mermaid)
    #svg_name = uuid.uuid4().hex.upper()[0:11]
    #print(svg_name)
    #status = validate_mermaid(svg_name,new_model)
    #   print(new_model)        print(status)
    return {'model': new_model}

@route('/description', method=['POST'])
# curl --header "Content-Type: application/json"   --request POST   --data '{"model":"graph LR\n1:startevent:((startevent)) --> 2:task:(Get money)\n2:task: --> 3:task:(Buy a new pair of shoes)\n3:task: --> 4:endevent:((endevent))","graph_type":"mermaid.js","gpt_model":"gpt-4"}'   http://0.0.0.0:7070/description
def create_description():
    try:
        data = request.json
    except:
        response.status = 400
    if data is None:
        response.status = 400

    gpt_model = data["gpt_model"]
    model = data['model']
    graph_type = data['graph_type']

    if model == "" or graph_type == "" or gpt_model == "":
        response.status = 400
        return {"message": "At least one of the parameters is missing!!!"}

    description = generate_description(gpt_model,model,graph_type,elements=0)
    return {'model': model, "description": description}


@route('/model_tasks', method=['GET','POST'])
# curl --header "Content-Type: application/json"   --request GET   --data '{"model":"<definitions><process><task name=\"new\"></task><task name=\"second\"></task></process></definitions>","model_format":"xml"}'   http://0.0.0.0:7070/model_tasks
def extract_model_tasks():
    data = request.json
    model = data["model"]
    model_format = data["model_format"]
    if model_format == "xml":
        tasks = extract_bpmn_tasks(model)
    elif model_format == "graphviz":
        model = model.splitlines()
        tasks = extract_graphviz_tasks(model)
    elif model_format == "mermaid":
        model = model.splitlines()
        tasks = extract_mermaid_tasks(model)
    else:
        response.status = 400
        return {"message": "Selected format is not supported. Please choose xml, mermaid or graphviz!!!"}
    return {'tasks': tasks}

@route('/file_tasks', method=['POST'])
# curl -i -X POST -H "Content-Type: multipart/form-data" -F "model=@/home/i17/Downloads/shoes.bpmn" -F "model_format=xml" http://0.0.0.0:7070/test_xml
def extract_model_tasks():
    upload_file = request.POST['model']
    model_format = request.POST['model_format']
    content = upload_file.file.read()
    if model_format == "xml":
        tasks = extract_bpmn_tasks(content)
    elif model_format == "graphviz":
        model = model.splitlines()
        tasks = extract_graphviz_tasks(model)
    elif model_format == "mermaid":
        model = model.splitlines()
        tasks = extract_mermaid_tasks(model)
    else:
        response.status = 400
        return {"message": "Selected format is not supported. Please choose xml, mermaid or graphviz!!!"}
    return {'tasks': tasks}

@route('/ask_llama', method=['POST'])
# curl --header "Content-Type: application/json"   --request POST   --data '{"prompt":"Do you know what is bpmn?"}'   http://0.0.0.0:7070/ask_llama
def ask_llama():
    try:
        data = request.json
    except:
        response.status = 400
    if data is None:
        response.status = 400

    prompt = data["prompt"]

    if prompt == "":
        response.status = 400
        return {"message": "The prompt is missing!"}

    response = single_prompt_llama(prompt)
    return {'prompt': prompt, "response": response}

@route('/multi_ask_llama', method=['POST'])
# curl --header "Content-Type: application/json"   --request POST   --data '{"prompts":["Do you know what is bpmn?","what is the sense of life?","do you feel yourself real?","do you want to be a human?"]}'   http://0.0.0.0:7070/ask_llama
def multi_ask_llama():
    try:
        data = request.json
    except:
        response.status = 400
    if data is None:
        response.status = 400

    prompt = data["prompts"]

    if prompt == "":
        response.status = 400
        return {"message": "The prompts are missing!"}

    response = multi_prompt_llama(prompt)
    output = {}
    for x in range(len(response)):
        output[prompt[x]] = response[x]

    return {'output': output}

@route('/send_emails', method=['GET','POST'])
def send_emails():
    try:
        emails = request.params["emails"]
        link = request.params["link"]
        s = smtplib.SMTP('smtp.gmail.com', 587)
        s.starttls()
        sender = "n.klievtsova@gmail.com"
        mypassword = "xrbd qmoj ncfq egya"
        s.login(sender, mypassword)
        for email in emails:
            message = "New evaluation report is generated. To access the details, kindly follow the link provided further: {}".format(link)
            s.sendmail(sender, email, message)
        s.quit()
        return {'status': "ok"}
    except:
        response.status = 400
        return {'status': "nok"}

@route('/save_model', method=['POST'])
def save_model():
    print("here")
    data = request.forms
    model = data['model']
    user_id = data['user_id']
    description = data['description']
    if model == "" or user_id == "" or description == "":
        response.status = 400
        return {"message": "At least one of the parameters is missing!!!"}
    dictionary = {
        "description": description,
        "model": model
    }
    output = {}
    output[user_id]=dictionary
    with open("../../user_data/{}.json".format(user_id), "w") as outfile:
        body = json.dumps(output, indent=4)
        outfile.write(body)

@route('/save_new_model', method=['POST'])
def save_new_model():
    data = request.forms
    counter = data['counter']
    user_id = data['user_id']
    if counter == "survey":
        general_info = data["general_info"]
        survey = data["survey"]
        openfile = open("../../user_data/{}.json".format(user_id),'r')
        data = json.load(openfile)
        data[user_id]["general_info"] = general_info
        data[user_id]["survey"] = survey 
    else:    
        model = data['model']
        update = data['update']
        if model == "" or user_id == "" or counter == "":
            response.status = 400
            return {"message": "At least one of the parameters is missing!!!"}
        openfile = open("../../user_data/{}.json".format(user_id),'r')
        data = json.load(openfile)
        key = "model{}".format(counter)
        prompt = "prompt{}".format(counter)
        data[user_id][key] = model
        data[user_id][prompt] = update
    json_object = json.dumps(data, indent=4)
    with open("../../user_data/{}.json".format(user_id), "w") as outfile:
        outfile.write(json_object)


