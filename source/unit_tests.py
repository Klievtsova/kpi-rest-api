from webtest import TestApp
from run import app

port = app.config['rest-api.port']
path = "http://0.0.0.0:{}".format(port)
testapp = TestApp(app)

def execute_all_tests():
    report = []
    import unit_tests
    for i in dir(unit_tests):
        if "_route" in i:
            item = getattr(unit_tests,i)
            if callable(item):
                output = item()
                report.append(output)
    print(report)
    return report

def test_route():
    route = "{}/test".format(path)
    response = testapp.get(route)
    assert response.status == '200 OK'
    return {"route":route,"response":response.status}

def tasks_route():
    route = "{}/tasks".format(path)
    data = dict(description="task one is get money, and task two is to buy a new pair of shoes.", gpt_model='gpt-3.5-turbo-instruct')
    response = testapp.post_json(route,data)
    assert response.status == '200 OK'
    return {"route":route,"response":response.status}

def task_list_route():
    route = "{}/task_list".format(path)
    data = dict(description="task one is get money, and task two is to buy a new pair of shoes.", gpt_model='gpt-3.5-turbo-instruct')
    response = testapp.post_json(route,data)
    assert response.status == '200 OK'
    return {"route":route,"response":response.status}

def kpis_route():
    route = "{}/kpis".format(path)
    response = testapp.get(route)
    assert response.status == '200 OK'
    return {"route":route,"response":response.status}

def text_similarity_route():
    route = "{}/kpis/text_similarity".format(path)
    data = dict(string_1="task one is get money, and task two is to buy a new pair of shoes.",string_2="There are 2 tasks in the process: get money and buy shoes.", sim_type="bert")
    response = testapp.post_json(route,data)
    assert response.status == '200 OK'
    return {"route":route,"response":response.status}

def set_overlap_route():
    route = "{}/kpis/set_overlap".format(path)
    data = dict(list_1=["go to the shop","buy shoes","go home"],list_2=["go home","buy bread"], sim_type="bert")
    response = testapp.post_json(route,data)
    assert response.status == '200 OK'
    return {"route":route,"response":response.status}

def readability_test_route():
    route = "{}/kpis/readability_test".format(path)
    data = dict(text="task one is get money, and task two is to buy a new pair of shoes.")
    response = testapp.post_json(route,data)
    assert response.status == '200 OK'
    return {"route":route,"response":response.status}

def quantitative_data_route():
    route = "{}/kpis/quantitative_data".format(path)
    data = dict(text="task one is get money, and task two is to buy a new pair of shoes.")
    response = testapp.post_json(route,data)
    assert response.status == '200 OK'
    return {"route":route,"response":response.status}

def augmentation_route():
    route = "{}/augmentation".format(path)
    data = dict(text="task one is get money, and task two is to buy a new pair of shoes.")
    response = testapp.post_json(route,data)
    assert response.status == '200 OK'
    return {"route":route,"response":response.status}

def model_route():
    route = "{}/model".format(path)
    data = dict(description="task one is get money, and task two is to buy a new pair of shoes.",graph_type="mermaid.js")
    response = testapp.post_json(route,data)
    assert response.status == '200 OK'
    return {"route":route,"response":response.status}

def description_route():
    route = "{}/description".format(path)
    data = dict(model="graph LR\n1:startevent:((startevent)) --> 2:task:(Get money)\n2:task: --> 3:task:(Buy a new pair of shoes)\n3:task: --> 4:endevent:((endevent))",graph_type="mermaid.js",gpt_model="gpt-4")
    response = testapp.post_json(route,data)
    assert response.status == '200 OK'
    return {"route":route,"response":response.status}

def model_tasks_route():
    route = "{}/model_tasks".format(path)
    data = dict(model="graph LR\n1:startevent:((startevent)) --> 2:task:(Get money)\n2:task: --> 3:task:(Buy a new pair of shoes)\n3:task: --> 4:endevent:((endevent))",model_format="mermaid")
    response = testapp.post_json(route,data)
    assert response.status == '200 OK'
    return {"route":route,"response":response.status}

def ask_llama_route():
    route = "{}/ask_llama".format(path)
    data = dict(prompt="Do you know what is bpmn?")
    response = testapp.post_json(route,data)
    assert response.status == '200 OK'
    return {"route":route,"response":response.status}





