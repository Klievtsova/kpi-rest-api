import os
import json
import pandas as pd

def get_average_model_tasks(file_name,search_val):
    file = open(file_name)  
    info = json.load(file)
    data = {}
    for key,value in info.items():
        sum = 0
        for v in value:
            num = value[v][search_val]
            sum = sum + num
        aver = sum/len(value)
        data[key] = aver
    return data

def get_average_chat_tasks(gptdir):
    table = {}
    for root, dirs, files in os.walk(gptdir):
        for f in files:
            path_to_file = os.path.join(root, f)
            extracted = open(path_to_file)
            extracted_chat = json.load(extracted)
            for key,value in extracted_chat.items():
                if key in table:
                    table[key] = value["amount"] + table[key]
                else:
                    table[key] = value["amount"] 
    for t in table:
            table[t] = table[t]/len(files)
    return table


def get_average_model_words(file_name):
    file = open(file_name)  
    info = json.load(file)
    data = {}
    for key,value in info.items():
        temp = []
        for v in value:
            tasks = value[v]["process"]
            amount = value[v]["amount"]
            if amount > 0:
                s = 0
                for t in tasks:
                    words = t.split(" ")
                    num = len(words)
                    s = s + num
                aver = s/amount
            temp.append(aver)
        data[key] = sum(temp)/len(temp)
    return data


def get_average_chat_words(gptdir):
    table = {}
    for root, dirs, files in os.walk(gptdir):
        for f in files:
            path_to_file = os.path.join(root, f)
            extracted = open(path_to_file)
            extracted_chat = json.load(extracted)
            for key,value in extracted_chat.items():
                tasks = value["tasks"]
                amount = value["amount"]
                s = 0
                for t in tasks:
                    words = t.split(" ")
                    num = len(words)
                    s = s + num
                aver = s/amount
                if key in table:
                    table[key] = aver + table[key]
                else:
                    table[key] = aver
            print(table)
    for t in table:
            table[t] = table[t]/len(files)
    return table

def get_augment_average(augmented_dir):
    for subdir, dirs, files in os.walk(augmented_dir):
        table = {}
        for file in files:
            print(file)
            set = os.path.join(augmented_dir, file)
            f = open(set)  
            set_models = json.load(f)
            for key, value in set_models.items():
                if key not in table:
                    table[key] = []
                table[key].append(value["amount"])

    test = pd.DataFrame(table)
    test["mean"] = test.mean(axis=1)
    test["method"] = ["eda_del","eda_ins","eda_rep","eda_swap","emb","nplaug","tran_de","tran_es","tran_ru"]
    return test