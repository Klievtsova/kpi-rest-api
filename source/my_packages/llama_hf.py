from transformers import AutoTokenizer
import transformers
import torch
import os
import json

def create_prompt(system_message,user_prompt):
    prompt = """<s>[INST]
    <<SYS>>{system_message}<</SYS>>
    {user_prompt} \n
    [/INST]""".format(system_message=system_message,user_prompt=user_prompt)
    return prompt

def create_prompts(system_message,user_prompts):
    prompts = []
    for user_prompt in user_prompts:
        prompt = create_prompt(system_message,user_prompt)
        prompts.append(prompt)
    return prompts

def load_llama_parameters():
    f = open('my_packages/llama_parameters.json')
    llama_parameters = json.load(f)
    return llama_parameters

def prepare_pipeline(pipeline,tokenizer,prompt):
    parameters = load_llama_parameters()
    sequences = pipeline(
        prompt,
        temperature=parameters["temperature"],
        do_sample=parameters["do_sample"],
        top_k=parameters["top_k"],
        top_p=parameters["top_p"],
        repetition_penalty=parameters["repetition_penalty"],
        num_return_sequences=parameters["num_return_sequences"],
        eos_token_id=tokenizer.eos_token_id,
        max_length=parameters["max_length"],
    )
    return sequences

def ask_llama(pipeline,tokenizer,prompt):
    sequences = prepare_pipeline(pipeline,tokenizer,prompt)
    for seq in sequences:
        response = seq['generated_text']
        response = response.split("[/INST]")
    return response[1]

def multi_ask_llama(pipeline,tokenizer,prompts):
    sequences = prepare_pipeline(pipeline,tokenizer,prompts)
    output = []
    for seq in sequences:
        response = seq[0]['generated_text']
        response = response.split("[/INST]")
        output.append(response[1])
    return output

def create_pipeline(model,multi=0):
    if multi == 0:
        pipeline = transformers.pipeline(
            "text-generation",
            model=model,
            torch_dtype=torch.float16,
            device_map="auto",
        )
    else:
        pipeline = transformers.pipeline(
            "text-generation",
            model=model,
            torch_dtype=torch.float16,
            device_map="auto",
            batch_size=4
        )
        pipeline.tokenizer.pad_token_id = pipeline.model.config.eos_token_id
    return pipeline

def single_prompt_llama(user_prompt):
    system_message = "\nYou are a helpful, respectful and honest assistant. Always answer as helpfully as possible, while being safe.  Your answers should not include any harmful, unethical, racist, sexist, toxic, dangerous, or illegal content. Please ensure that your responses are socially unbiased and positive in nature.\n\nIf a question does not make any sense, or is not factually coherent, explain why instead of answering something not correct. If you don't know the answer to a question, please don't share false information."
    full_prompt = create_prompt(system_message,user_prompt)
    model = "meta-llama/Llama-2-7b-chat-hf"
    tokenizer = AutoTokenizer.from_pretrained(model)
    pipeline = create_pipeline(model,0)
    generated_text = ask_llama(pipeline,tokenizer,full_prompt)
    return generated_text

def multi_prompt_llama(user_prompts):
    system_message = "\nYou are a helpful, respectful and honest assistant. Always answer as helpfully as possible, while being safe.  Your answers should not include any harmful, unethical, racist, sexist, toxic, dangerous, or illegal content. Please ensure that your responses are socially unbiased and positive in nature.\n\nIf a question does not make any sense, or is not factually coherent, explain why instead of answering something not correct. If you don't know the answer to a question, please don't share false information."
    full_prompts = create_prompts(system_message,user_prompts)
    model = "meta-llama/Llama-2-7b-chat-hf"
    tokenizer = AutoTokenizer.from_pretrained(model)
    pipeline = create_pipeline(model,1)
    generated_text = multi_ask_llama(pipeline,tokenizer,full_prompts)
    return generated_text
