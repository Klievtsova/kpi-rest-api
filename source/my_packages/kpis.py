from my_packages.similarity import get_cosine,sts_bert,matrix_output
from sentence_transformers import SentenceTransformer
import nltk
nltk.download('punkt')
from nltk import sent_tokenize
from nltk import word_tokenize
import textstat
import os
import pandas as pd

def get_similarity(text1,text2,type="cos"):
    if type == "cos":
        similarity = get_cosine(text1,text2)
    else:
        similarity = sts_bert(text1,text2)
    return similarity

"""both description and llm_tasks are string values"""
def text_similarity(description,llm_tasks,type="cos"):
    similarity = get_similarity(description,llm_tasks,type)
    return {"text_similarity":similarity}

"""llm_tasks is a list, bpmn_task_list is a dictionaty of lists (length: 1..n)"""
def set_similatiry(llm_tasks,bpmn_tasks_list,type="cos"):
    kpis = []
    bpmn_lens = []
    llm_length = len(llm_tasks)
    sllm_tasks = '. '.join(llm_tasks)
    for bpmn_tasks in bpmn_tasks_list:
        bpmn_length = bpmn_tasks_list[bpmn_tasks]["amount"]
        sbpmn_tasks = '. '.join(bpmn_tasks_list[bpmn_tasks]["process"])
        similarity = get_similarity(sllm_tasks,sbpmn_tasks,type)
        kpis.append(similarity)
        bpmn_lens.append(bpmn_length)
    kpi = sum(kpis)/len(kpis)
    aver_bpmn = sum(bpmn_lens)/len(bpmn_lens)
    return {"set_similarity":kpi,"number_tasks_llm":llm_length,"number_tasks_bpmn":aver_bpmn}

"""llm_tasks is a list, bpmn_task_list is a dictionaty of lists (length: 1..n)"""
def simple_set_similatiry(llm_tasks,bpmn_tasks,type="cos"):
    llm_length = len(llm_tasks)
    sllm_tasks = '. '.join(llm_tasks)
    bpmn_length = len(bpmn_tasks)
    sbpmn_tasks = '. '.join(bpmn_tasks)
    similarity = get_similarity(sllm_tasks,sbpmn_tasks,type)
    return {"set_similarity":similarity,"number_tasks_llm":llm_length,"number_tasks_bpmn":bpmn_length}

"""bpmn_tasks and llm_tasks are lists of strings"""
def set_overlap_rest(llm_tasks,bpmn_tasks,type="cos"):
    sim_model = SentenceTransformer('sentence-transformers/stsb-mpnet-base-v2')
    if len(llm_tasks)==0 and len(bpmn_tasks)==0:
        return {"aligned_list1":0, "aligned_list2":0, "misaligned_list1": 0, "misaligned_list2": 0}
    elif len(llm_tasks)==0:
        return {"aligned_list1":0, "aligned_list2":0, "misaligned_list1": 0, "misaligned_list2":  len(bpmn_tasks)}
    elif len(bpmn_tasks)==0:
        return {"aligned_list1":0, "aligned_list2":0, "misaligned_list1": len(llm_tasks), "misaligned_list2":0}
    overlap = matrix_output(llm_tasks,bpmn_tasks,type,sim_model)
    only_model = overlap["orig_tasks_model"] - overlap["tasks_model"]
    only_llm = overlap["orig_tasks_chat"] - overlap["tasks_chat"]
    absolut = {"aligned_list1":overlap["tasks_chat"], "aligned_list2":overlap["tasks_model"], "misaligned_list1": only_llm, "misaligned_list2": only_model}
    ratio = {"aligned_list1":overlap["tasks_chat"]/(overlap["tasks_chat"]+only_llm), "aligned_list2":overlap["tasks_model"]/(overlap["tasks_model"]+only_model), "misaligned_list1": only_llm/(overlap["tasks_chat"]+only_llm), "misaligned_list2": only_model/(overlap["tasks_model"]+only_model)}
    return {"absolut":absolut,"percentage":ratio }


def count_sentences(text):
    sentences = sent_tokenize(text)
    return len(sentences)

def get_sentences(text):
    sentences = sent_tokenize(text)
    return sentences
    
def count_words(sentence):
    words = word_tokenize(sentence)
    return len(words)

def readability(text):
    flesch_kincaid_grade = textstat.flesch_kincaid_grade(text)
    flesch_reading_ease = textstat.flesch_reading_ease(text)
    return {'grade':flesch_kincaid_grade,'ease':flesch_reading_ease}

def quantitative(text):
    num_sentences = count_sentences(text)
    num_words = count_words(text)
    aver = num_words/num_sentences
    return {'N_sentences':num_sentences,'N_words':num_words,'w/s':aver}
    


        