from openai import OpenAI
from rest_api import openai_key 
import requests
import json

open_app = OpenAI(
    api_key=openai_key
)



parameters = {
    "temperature":0,
    "max_tokens":1024,
    "top_p": 1
}

def ask_gpt_complete(prompt,model,parameters):
    try:
        response = open_app.completions.create(
            model=model,
            prompt = prompt,
            max_tokens=parameters["max_tokens"],
            temperature = parameters["temperature"],
            top_p = parameters["top_p"],
            stop = ["ooooooooooo"]
            )
        return response.choices[0].text
    except Exception as e:
        print("---smth went wrong",e)
        return "smth went wrong"

def ask_gpt_chat(prompt,model,parameters):   
    try:
        response = open_app.chat.completions.create(
            model=model,
            messages=[
                {"role": "user", "content": prompt}
            ],
            temperature=parameters["temperature"],
            max_tokens=parameters["max_tokens"],
            top_p=parameters["top_p"],
            frequency_penalty=0,
            presence_penalty=0
        )
        return response.choices[0].message.content
    except Exception as e:
        print("smth went wrong",e)
        return "smth went wrong"   

def ask_gpt_system(prompt,model,rules):
    print("here")
    #try:
    response = open_app.chat.completions.create(
        model=model,
        messages=[
            {"role": "user", "content": prompt},
            {"role": "system", "content": "{}. Return only mermaid.js code with no additional text.".format(rules) }
        ],
        temperature=parameters["temperature"],
        max_tokens=parameters["max_tokens"],
        top_p=parameters["top_p"],
        frequency_penalty=0,
        presence_penalty=0
    )
    return response.choices[0].message.content
    #except Exception as e:
    #    print("smth went wrong",e)
    #    return "smth went wrong" 

def ask_gpt(prompt,model):
    if "instruct" in model:
        response = ask_gpt_complete(prompt,model,parameters)
    else:
        response = ask_gpt_chat(prompt,model,parameters)
    return response

def extract_tasks(model,original,restricted):
    try:
        if restricted == 0:
            prompt = "Considering following process description [{}] return the list of main tasks in it (each from new line).".format(original) 
        else:
            prompt = "Considering following process description [{}]  return the list of activities (each 3-5 words maximum) one by one without any additional information.".format(original) 
        response = ask_gpt(prompt,model)
        return response
    except Exception as e:
        print("smth went wrong",e)
        return "smth went wrong"

def convert_to_list(tasks):
    task_list = tasks.split("\n")
    task_list = [x for x in task_list if x != '']
    amount = len(task_list)
    clean_tasks = []
    for t in task_list:
        try: 
            t = t.split(".")[1]
            clean_tasks.append(t)
        except:
            clean_tasks.append(t)

    result = {"amount":amount,"tasks":clean_tasks}
    return result

def generate_description(model,original,graph_type,elements=0):
    try:
        if elements == 1:
            prompt = "Read this {} model: {}. Convert this model to a textual process description using simple natural language. Return only text summary".format(graph_type,original) 
        else:
            prompt = "Read this {} model: {}. Convert this model to a textual process description using simple natural language without mentioning types of the model elements (i.e., task, startevent, endevent,gateway, etc.). Return only text summary".format(graph_type,original) 
        response = ask_gpt(prompt,model)
        return response
    except Exception as e:
        print("smth went wrong",e)
        return "smth went wrong"
